---
title: Premium Subscription
subtitle: Hear Me Now App
comments: false
---

All users of HearMeNow can send and receive an unlimited number of notifications as well as can have an unlimited number of secondary devices.

Non-premium users could only add one trusted contact.

If you would like to add more than one trusted contact please subscribe. Our premium subscription would allow you to:

* Add an unlimited number of trusted contacts
* Give permission to draw your attention to an unlimited number of trusted contacts
* Send notifications to an unlimited number of trusted contacts

For more information concerning Payments, Subscriptions and Refund Policy visit our [Terms of Use]({{< ref "/page/terms-of-use" >}}).
