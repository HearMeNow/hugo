---
title: Datenschutzerklärung
subtitle: Hear Me Now
comments: false
---

Die Privatsphäre unserer Kunden ist eine unserer Hauptprioritäten. HearMeNow weiß, dass es Ihnen wichtig ist, wie Informationen über Sie verwendet und weitergegeben werden, und wir danken Ihnen für Ihr Vertrauen, dass wir dies sorgfältig und vernünftig tun. Diese Datenschutzerklärung erklärt wie die Entwickler von HearMeNow ("wir", "wir", "unsere") die Daten verwenden und schützen, die Sie uns zur Verfügung stellen, wenn Sie HearMeNow ("Applikation", "Service") verwenden.

Wir behalten uns das Recht vor, diese Datenschutzerklärung jederzeit zu ändern. Wenn wir wesentliche Änderungen vornehmen, werden Sie umgehend per E-Mail darüber informiert, damit Sie die Änderungen überprüfen können, bevor sie in Kraft treten. Wir empfehlen Ihnen, diese Seite regelmäßig zu besuchen, um die neueste Version dieser Datenschutzerklärung anzuzeigen. Wenn Sie unsere Applikation weiterhin nutzen, stimmen Sie der neuesten Version der Datenschutzerklärung zu. Wenn Sie die Änderungen ablehnen, können Sie die Applikation deinstallieren und den Zugriff auf unsere Servise beenden.

Wenn Sie weitere Fragen haben oder weitere Informationen zu unseren Datenschutzerklärung benötigen, wenden Sie sich bitte per E-Mail an HearMeNowApp@gmail.com.


## Welche Benutzerdaten sammeln wir und wie verwenden wir die gesammelten Informationen?

### Ihre Emailadresse
Damit Ihre vertrauenswürdigen Kontakte Ihre Aufmerksamkeit auf Ihr Gerät lenken können, verwendet HearMeNow E-Mail, um Benutzer zu identifizieren. Wir behalten uns das Recht vor, Sie per E-Mail über die Aktualisierungen dieser Datenschutzerklärung zu informieren.

### Ihre Benachrichtigungen
Wir speichern Ihre Benachrichtigungshistorie nicht im normalen Verlauf unserer Service. Sobald Ihre Benachrichtigungen (z. B. wenn Sie die Aufmerksamkeit einer Person auf sich ziehen) übermittelt wurden, werden diese von unseren Servern gelöscht. Ihr Benachrichtigungsverlauf wird nur auf Ihrem eigenen Gerät gespeichert. Wenn eine Benachrichtigung nicht sofort zugestellt werden kann (z. B. wenn Sie offline sind), können wir sie auf unseren Servern bis zu 30 Tage aufbewahren, während wir versuchen, sie zuzustellen. Wenn eine Nachricht nach 30 Tagen immer noch nicht zugestellt wird, löschen wir sie.

### Ihre Kontaktliste
Wir speichern keine Informationen über Ihre Kontakte auf unseren Servern. Um Ihre Kontaktinformationen korrekt anzuzeigen und die Kontaktinformationen auf dem neuesten Stand zu halten, benötigt HearMeNow Zugriff auf Ihre Kontaktliste. Kontaktinformationen werden jedoch nur auf Ihrem eigenen Gerät gespeichert. Sie können den Zugriff von HearMeNow auf Ihre Kontaktliste in den Einstellungen Ihres Handys jederzeit sperren.

### Kundendienst
Sie können uns Informationen zu Ihrer Nutzung von HearMeNow per E-Mail, z. B. Screenshots, und Ihre Kontaktinformationen, zur Verfügung stellen, damit wir Sie beim Kundendienst unterstützen können. Wir behalten uns das Recht vor, diese Informationen auf unseren Servern zu speichern und Ihr Feedback zur weiteren Verbesserung unserer Services zu verwenden.

### Log Data
Um die Leistung und die Benutzerfreundlichkeit von HearMeNow zu bewerten und zu verbessern, erfassen wir automatisch anonymisierte Protokolldaten (teilweise über Drittanbieter), die Informationen wie die IP-Adresse (Internet Protocol) Ihres Geräts, den Gerätenamen, die Systemversion, die System Konfiguration, die Uhrzeit und das Datum Ihrer Nutzung von HearMeNowApp, Gerätestatusinformationen, eindeutige Gerätekennungen, Standortdaten, Nutzungsdaten und andere Statistiken.

### Zahlungsinformationen
Details Ihrer Zahlungsart (z. B. Ihre Kreditkartennummer, Bankverbindung, PayPal-Anmeldeinformationen oder andere) werden von HearMeNow nicht gespeichert, da alle Zahlungen von Google Play abgewickelt werden. In den Datenschutzerklärung von [Google Play Services](https://policies.google.com/privacy?hl=de-DE) erfahren Sie, wie Ihre Zahlungsdaten erfasst, gespeichert und verarbeitet werden.

### Drittanbieter
Wir arbeiten mit Drittanbietern zusammen, die helfen uns HearMeNow zu betreiben, bereitzustellen, zu verbessern, zu verstehen, anzupassen, zu unterstützen und zu vermarkten. Diese Unternehmen können uns unter bestimmten Umständen Informationen über Sie zur Verfügung stellen. Beispielsweise können Application Stores Berichte bereitstellen, die uns bei der Diagnose und Behebung von Serviceproblemen helfen.

Ihre Daten können in die USA oder in ein anderes Land übertragen, dort gespeichert und verarbeitet werden, wo diese Drittanbieter tätig sind.

Wir verwenden die folgenden Dienste, um die Leistung der Applikation und Ihre Benutzererfahrung zu verbessern:


* Firebase Crashlytics: Dieser Service wird von Fabric (einem Unternehmen von Google) bereitgestellt und hilft uns zu verstehen, warum und wann HearMeNow abstürzt, und gibt uns Informationen zu Fehlern. Dafür verwendet Crashlytics Ihre Protokolldaten. Diese Informationen werden automatisch gesammelt und ermöglichen keine Identifizierung des Endbenutzers. Weitere Informationen zu Datenschutzerklärung von Crashlytics  finden Sie [hier](https://fabric.io/terms?utm_campaign=crashlytics-marketing&utm_medium=natural).
* Google Analytics für Firebase: Dieser Service wird von Google bereitgestellt und gibt uns einen Einblick in die Nutzung der Applikation und die Nutzerinteraktion. Google Analytics für Firebase sammelt Informationen automatisch und die gesammelten Informationen ermöglichen keine Identifizierung des Endbenutzers.
* Google Play Services: Wir verwenden die von Google Play bereitgestellten Services, um unsere Applikation zu vermarkten und Ihre Abonnements zu verarbeiten. Um mehr darüber zu erfahren, welche Informationen von Google gesammelt werden und wie Google diese Informationen verarbeitet, klicken Sie [hier](https://policies.google.com/privacy?hl=de-DE).

Firebase Crashlytics und Google Analytics for Firebase erfassen Ihre Daten anonym. Die Zusammenführung von personenbezogenen Daten mit nicht personenbezogenen Daten, die über Google Analytics für Firebase und Crashlytics erfasst wurden, wird nicht unterstützt. Sie können die Erfassung von Daten durch Crashlytics und Google Analytics für Firebase in Ihren Applikationseinstellungen deaktivieren.

## Schutz und Sicherung der Daten
Wir schätzen Ihr Vertrauen, dass Sie uns Ihre persönlichen Daten zur Verfügung stellen. Daher bemühen wir uns, wirtschaftlich akzeptable Schutzmaßnahmen zu ergreifen. Die Entwickler von HearMeNow verpflichten sich, Ihre Daten zu schützen und vertraulich zu behandeln. Wir haben alles in unserer Macht Stehende getan, um Datendiebstahl, unbefugten Zugriff und Offenlegung durch die Implementierung der neuesten Technologien und Software zu verhindern und von uns gesammelten Informationen zu schützen.

## Children’s Privacy
Wir sammeln nicht wissentlich personenbezogene Daten von minderjährigen Personen, wie in unseren [Nutzungsbedingungen]({{< ref "/page/terms-of-use" >}}) angegeben. Wenn wir feststellen, dass eine minderjährige Person uns ohne Zustimmung ihrer Eltern oder Erziehungsberechtigten personenbezogene Daten zur Verfügung gestellt hat, löschen wir diese Informationen unverzüglich von unseren Servern. Wenn Sie Elternteil oder Erziehungsberechtigter sind und wissen, dass Ihr Kind uns ohne Ihre Zustimmung personenbezogene Daten zur Verfügung gestellt hat, kontaktieren Sie uns bitte über HearMeNowApp@gmail.com.

## So löschen Sie Ihre Daten oder entziehen Ihre Berechtigungen
Sie können HearMeNows Zugriff auf Kontakte jederzeit in Ihren Handyeinstellungen entziehen und Google Analytics für Firebase und Crashlytics in den Einstellungen der Applikation deaktivieren. Wenn Sie möchten, dass wir Ihre Daten nicht mehr erfassen und Ihr Konto deaktivieren, deinstallieren Sie einfach die Applikation. Wenn Sie Ihre Daten dauerhaft löschen möchten, senden Sie bitte eine E-Mail-Anfrage mit dem Betreff „Erase my account“ an HearMeNowApp@gmail.com.

Bitte beachten Sie, dass im Falle einer Diskrepanz zwischen der englischen und der deutschen Version der Datenschutzrichtlinie die englische Version des Textes Vorrang hat.

Dieses Dokument wurde zuletzt am 1. März 2020 aktualisiert.
