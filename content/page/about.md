---
title: About Us
subtitle: Hear Me Now App
comments: false
---

## HearMeNow: because some calls are too important to miss!

There are many good reasons to turn off the sound on your phone: being at the meeting, sharing a crowded office, or craving some quiet time. Even though we may not want to be disturbed in certain situations, we don’t want to miss urgent calls or texts from our friends and family either.

We are a team of two - Nikita and Tanya - who always want to be able to reach each other. That’s why we developed HearMeNow: an Android app that helps us stay connected even when our phones are on mute. It helps us to contact each other even when, say, a phone got lost  in the depth of a backpack or when we are being carried away with our daily activities. We developed HearMeNow because some calls are too important to miss!
