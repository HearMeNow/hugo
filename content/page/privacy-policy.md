---
title: Privacy Policy
subtitle: Hear Me Now
comments: false
---

One of our main priorities is the privacy of our customers. HearMeNow knows that you care how information about you is used and shared, and we appreciate your trust that we will do so carefully and sensibly. This privacy policy ("policy") will help you understand how HearMeNow’s Developers ("us", "we", "our") uses and protects the data you provide to us when you use HearMeNow ("Application", "Service").

We reserve the right to change this policy at any given time and if we make material changes to it you will be promptly updated by email to provide you the opportunity to review the changes before they become effective. If you want to make sure that you are up to date with the latest changes, we advise you to frequently visit this page. By continuing to use our Application  you agree to the latest version of the Policy. If you object to any changes, you can uninstall the Application and stop accessing or otherwise using our Services.

If you have additional questions or require more information about our Privacy Policy, do not hesitate to contact us through email at HearMeNowApp@gmail.com.

## What User Data We Collect And How do We Use the Collected Information?

### Your email address
To allow your trusted contacts draw your attention by targeting your device, HearMeNow uses email to identify users. We reserve ourselves the right to notify you about the updates to this Privacy Policy by email.

### Your Notifications
We do not retain your notification history in the ordinary course of providing our Services to you. Once your notifications (e.g. when you draw someone’s attention) are delivered, they are deleted from our servers. Your notification history is stored on your own device. If a notification cannot be delivered immediately (for example, if you are offline), we may keep it on our servers for up to 30 days as we try to deliver it. If a message is still undelivered after 30 days, we delete it.

### Your Connections
We do not retain any information about your contacts on our servers. In order to display your contact information correctly and to keep contact information up to date HearMeNow requires access to your contact list. However, contract information is stored only on your own device. You can always retract HearMeNow’s access to your contact list in your phone’s settings.

### Customer Support
You may provide us with information related to your use of HearMeNow by email, for example, screenshots, and how to contact you so we can provide you customer support. We reserve the right to retain this information on our servers and use your feedback to further improve our Services.

### Log Data
To evaluate and improve HearMeNow’s performance and user experience we automatically collect anonymized Log Data (partially through third-party service providers), which may include information such as your device Internet Protocol (IP) address, device name, operating system version, the configuration of the Application when utilizing HearMeNow, the time and date of your use of the Service, device state information, unique device identifiers, location data, usage data, and other statistics.

### Payment information
Details of your payment method (e.g. your credit card number, bank details, PayPal credentials or other) are not stored by HearMeNow, as all the payments are handled by Google Play. Check the privacy policy of [Google Play Services](https://policies.google.com/privacy?hl=en-US) to learn more about how your payment data are collected, stored and processed.

### Third-Party Service Providers
We work with third-party service providers to help us operate, provide, improve, understand, customize, support, and market HearMeNow. These companies may provide us information about you in certain circumstances; for example, Application stores may provide us reports to help us diagnose and fix service issues.

Your data may be transferred to, stored and processed in the United States or any other country where these third-party service providers operate.

We use the following services to improve the Application’s performance and your user experience:

* Firebase Crashlytics: This service is provided by Fabric (a Google company) and it helps us to understand why and when HearMeNow crashes and gives us information about bugs using your Log Data. This information is collected automatically and does not allow to identify the end-user. For more information about Crashlytics policies click [here](https://fabric.io/terms?utm_campaign=crashlytics-marketing&utm_medium=natural).
* Google Analytics for Firebase: This service is provided by Google and it gives us insight on the Application usage and user engagement. Google Analytics for Firebase collects information automatically and the collected information does not allow to identify the end-user.
* Google Play Services: We use the services provided by Google Play to distribute our Application and to process your subscriptions. To learn more about which information is collected by Google and how Google processes this information click [here](https://policies.google.com/privacy?hl=en-US).

Firebase Crashlytics and Google Analytics for Firebase collect your data anonymously. We do not facilitate the merging of personally-identifiable information with non-personally identifiable information obtained via Google Analytics for Firebase and Crashlytics. You can opt out of collecting data by Crashlytics and Google Analytics for Firebase in your application settings.

## Safeguarding and Securing the Data
We value your trust in providing us with your Personal Information, thus we are striving to use commercially acceptable means of protecting it. HearMeNow’s Developers are committed to securing your data and keeping it confidential. We have done all in our power to prevent data theft, unauthorized access, and disclosure by implementing the latest technologies and software, which help us safeguard all the information we collect.

## Children’s Privacy
HearMeNow’s Developers do not knowingly collect personally identifiable information from underaged individuals as specified in our [Terms of Use]({{< ref "/page/terms-of-use" >}}). In the case we discover that an underaged person has provided us with personal information without consent of their parents or guardians, we immediately delete this information from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information without your approval, please contact us via HearMeNowApp@gmail.com.

## How to erase your data or withdraw your permissions
You can always withdraw HearMeNow’s access to Contacts in your phone settings and you can opt out of Google analytics for Firebase and Crashlytics in the Application’s settings. If you wish us to stop collecting your data and deactivate your account, simply uninstall the Application. If you would like to permanently erase your data, please send an email request to HearMeNowApp@gmail.com with the subject “Erase my account”.

Please note that in case there is any discrepancy between the English and German versions of the Privacy Policy, the English version of the text shall prevail.

This document was last updated on 1st of March 2020.
