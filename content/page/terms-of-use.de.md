---
title: Terms Of Use
subtitle: Hear Me Now App
comments: false
---

These terms of use ("Terms", "Agreement") are an agreement between HearMeNow’s Developer ("HearMeNow’s Developer", "us", "we" or "our") and you ("User", "you" or "your"). This Agreement sets forth the general terms and conditions of your use of HearMeNow mobile application and any of its products or services (collectively, "HearMeNow", “Application” or "Services").

All users of HearMeNow are obliged to agree with these Terms before using our Services. You agree to the Terms and enter into a legally binding agreement with the HearMeNow’s developer by  clicking  “Log in with Google” or similar, registering, accessing or using our services. If you do not agree to these Terms do not click “Log in with Google” (or similar) and do not access or otherwise use any of our Services. You can terminate this Agreement at any time  by uninstalling the Application and not using our Services.

You understand that when using the HearMeNow Service you will be receiving  notifications from your trusted contacts (whom you grant permission to notify you, aka to “Draw your attention”) that might be untimely or objectionable and you agree that HearMeNow’s Developer is not responsible for the accuracy, usefulness, or safety of or relating to such notifications, and that such notifications are not the responsibility of HearMeNow’s Developer.

## Accounts and Membership
If you live in a country in the European Region, you must be at least 16 years old to use HearMeNow or such greater age required in your country to register for or use our Services. If you live in any other country except those in the European Region, you must be at least 13 years old to use our Services or such greater age required in your country to register for or use our Services.  By using this Application and by agreeing to this Agreement you warrant and represent that you are of the appropriate age. In addition to being of the minimum required age to use our Services under applicable law, if you are not old enough to have authority to agree to our Terms in your country, your parent or guardian must agree to our Terms on your behalf.

If you create an account in the HearMeNow, you are responsible for maintaining the security of your account and you are fully responsible for all activities that occur under the account and any other actions taken in connection with it. We may, but have no obligation to, monitor and review new accounts before you may sign in and use our Services. Providing false contact information of any kind may result in the termination of your account. You must immediately notify us of any unauthorized uses of your account or any other breaches of security. We will not be liable for any acts or omissions by you, including any damages of any kind incurred as a result of such acts or omissions. We may suspend, disable, or delete your account (or any part thereof) if we determine that you have violated any provision of this Agreement or that your conduct or content would tend to damage our reputation and goodwill. If we delete your account for the foregoing reasons, you may not re-register for our Services. We may block your email address and Internet protocol address to prevent further registration.

## The Privacy Policy
HearMeNow collects and uses your data to be able to reliably provide our services and improve user experience. To learn how we collect and use your information, and how you can control the information you share with us visit our [Privacy Policy]({{< ref "page/privacy-policy" >}}).

## Backups
We are not responsible for Content residing in the HearMeNow. In no event shall we be held liable for any loss of any Content. It is your sole responsibility to maintain appropriate backup of your Content. Notwithstanding the foregoing, on some occasions and in certain circumstances, with absolutely no obligation, we may be able to restore some or all of your data that has been deleted as of a certain date and time when we may have backed up data for our own purposes. We make no guarantee that the data you need will be available.

## Links to Other Applications or Services
Although HearMeNow may link to other mobile applications or services, we are not, directly or indirectly, implying any approval, association, sponsorship, endorsement, or affiliation with any linked mobile application, unless specifically stated herein. We are not responsible for examining or evaluating, and we do not warrant the offerings of, any businesses or individuals or the content of their mobile applications nor are we monitoring any transaction between you and the third party providers. We do not assume any responsibility or liability for the actions, products, services, and content of any other third-parties. You should carefully review the legal statements and other conditions of use of any mobile application which you access through a link from HearMeNow. You follow links to any other off-site mobile applications at your own risk.

## Prohibited Uses
In addition to other terms as set forth in the Agreement, you are prohibited from using HearMeNow or its Content:

1. for any unlawful purpose;
2. to solicit others to perform or participate in any unlawful acts;
3. to violate any international, federal, provincial or state regulations, rules, laws, or local ordinances;
4. to infringe upon or violate our intellectual property rights or the intellectual property rights of others;
5. to harass, abuse, insult, harm, defame, slander, disparage, intimidate, or discriminate based on gender, sexual orientation, religion, ethnicity, race, age, national origin, or disability;
6. to submit false or misleading information;
7. to upload or transmit viruses or any other type of malicious code that will or may be used in any way that will affect the functionality or operation of the Service or of any related mobile application, other mobile applications, or the Internet;
8. to collect or track the personal information of others;
9. to spam, phish, pharm, pretext, spider, crawl, or scrape;
10. for any obscene or immoral purpose;
11. to reverse engineer, decompile, disassemble, decipher or otherwise attempt to derive the source code for HearMeNow or any related technology that is not open source;
12. to use bots or other automated methods to access the Services, add or download contacts, send or redirect messages;
13. to monitor the HearMeNow’s availability, performance or functionality for any competitive purpose;
14. to engage in “framing,” “mirroring,” or otherwise simulating the appearance or function of the HearMeNow;
15. to overlay or otherwise modify the Services or their appearance (such as by inserting elements into the Services or removing, covering, or obscuring an advertisement included on the Services);
16. to duplicate, transfer, give access to, copy or distribute any part of the Service in any medium without HearMeNow Developer’s prior written authorization;
17. to interfere with or circumvent the security features of the Service or any related mobile application, other mobile applications, or the Internet.


We reserve the right to terminate your use of the Service or any related mobile application for violating any of the prohibited uses.

## Intellectual Property Rights
This Agreement does not transfer to you any intellectual property owned by HearMeNow’s Developer or third-parties, and all rights, titles, and interests in and to such property will remain (as between the parties) solely with HearMeNow Developer. All trademarks, service marks, graphics and logos used in connection with HearMeNow, are trademarks of HearMeNow Developer. Other trademarks, service marks, graphics and logos used in connection with HearMeNow may be the trademarks of other third-parties. Your use of HearMeNow grants you no right or license to reproduce, rent, lease, loan, trade, sell/re-sell or otherwise monetize HearMeNow or otherwise use any HearMeNow Developer or third-party trademarks. The Service is provided to you AS IS for your information and personal use only.

## Payment
Certain features are only available to users who have subscribed for Premium Account and who pay for it. You will find all the benefits and the features of Premium Accounts on our [webpage]({{< ref "/page/premium" >}}).

If you subscribe to a Premium Account, you agree that:

1. payment for using HearMeNow Premium Account is made in accordance with the invoicing conditions selected by you in Google Play;
2. the indicated price and discounts that apply and are accepted by all parties at the moment of transaction are indicated;
3. all charges and prices stated include the applicable value added tax calculated by Google Play services based on the billing information that you provided at the time of purchase and the charges may be subject to foreign exchange fees or differences in prices based on location (e.g. exchange rates);
4. if you subscribe, you will be automatically charged at the start of each subscription period for the fees and taxes applicable to that period;
5. to avoid future charges, cancel before the renewal date;
6. you may only offset invoices against legally determined or undisputed claims or right of retention. You may only assign claims arising from this agreement with our written consent.

Failure to pay Premium subscription fees will result in termination of the Premium Services.  In this case, you will remain obliged to reimburse the outstanding amount. We reserve the right to assert further claims for default of payment.

## No Warranty
You agree that your use of our Mobile Application or Services is solely at your own risk. You agree that such Service is provided on an "as is" and "as available" basis. We expressly disclaim all warranties of any kind, whether express or implied, including but not limited to the implied warranties of merchantability, fitness for a particular purpose and non-infringement. We make no warranty that the Services will meet your requirements, or that the Service will be uninterrupted, timely, secure, or error-free; nor do we make any warranty as to the results that may be obtained from the use of the Service or as to the accuracy or reliability of any information obtained through the Service or that defects in the Service will be corrected. You understand and agree that any material and/or data downloaded or otherwise obtained through the use of the Service is done at your own discretion and risk and that you will be solely responsible for any damage to your computer system or loss of data that results from the download of such material and/or data. We make no warranty regarding any goods or services purchased or obtained through the Service or any transactions entered into through the Service. No advice or information, whether oral or written, obtained by you from us or through the Service shall create any warranty not expressly made herein. HearMeNow’s developer assumes no liability or responsibility for any personal injury or property damage, of any nature whatsoever, resulting from your access to and use of our service nor we are responsible for any unauthorized access to or use of our servers and/or any and all personal information and/or financial information stored therein.

## Refund Policy
HearMeNow’s Developer will not be responsible for the downtimes of HearMeNow due to problems outside of our area of influence, such as internet/network connection problems, fault of third parties, force majeure. You are not entitled to damages caused by defect to HearMeNow due to a circumstance for which we are not responsible, either existing when you sign the agreement or occurring thereafter. You will be timely informed about maintenance work in HearMeNow Application in case HearMeNow Service will become temporarily unavailable to you. We are only liable for defects in HearMeNow provided they are not caused by limited access to the functionality during maintenance work. If use of HearMeNow is restricted due to a defect other than maintenance work, you will be released from paying your charges until the defect is corrected. If use is partially restricted, the charges will be reduced for the period until the defect is corrected. You must inform us of the defect by email immediately. Further claims and rights for defects to HearMeNow will not be recognized except if we are held liable for them by statutory provisions.

## Limitation of Liability
To the fullest extent permitted by applicable law, in no event will HearMeNow’s Developers, their affiliates, officers, directors, employees, agents, suppliers or licensors be liable to any person for any indirect, incidental, special, punitive, cover or consequential damages (including, without limitation, damages for lost profits, revenue, sales, goodwill, use of content, impact on business, business interruption, loss of anticipated savings, loss of business opportunity) however caused, under any theory of liability, including, without limitation, contract, tort, warranty, breach of statutory duty, negligence or otherwise, even if HearMeNow’s Developer has been advised as to the possibility of such damages or could have foreseen such damages. To the maximum extent permitted by applicable law, the aggregate liability of HearMeNow Developer and its affiliates, officers, employees, agents, suppliers and licensors, relating to the services cannot exceed the amount of ten dollars ($10.00) or any amounts actually paid in cash by you to HearMeNow’s Developer for the prior one month period prior to the first event or occurrence giving rise to such liability. The limitations and exclusions also apply if this remedy does not fully compensate you for any losses or fails of its essential purpose.

## Indemnification
You agree to indemnify and hold HearMeNow Developer and its affiliates, directors, officers, employees, and agents harmless from and against any liabilities, losses, damages or costs, including reasonable attorneys' fees, incurred in connection with or arising from any third-party allegations, claims, actions, disputes, or demands asserted against any of them as a result of or relating to your Content, your use of the HearMeNow or any willful misconduct on your part.

## Severability
All rights and restrictions contained in this Agreement may be exercised and shall be applicable and binding only to the extent that they do not violate any applicable laws and are intended to be limited to the extent necessary so that they will not render this Agreement illegal, invalid or unenforceable. If any provision or portion of any provision of this Agreement shall be held to be illegal, invalid or unenforceable by a court of competent jurisdiction, it is the intention of the parties that the remaining provisions or portions thereof shall constitute their agreement with respect to the subject matter hereof, and all such remaining provisions or portions thereof shall remain in full force and effect.

## Dispute Resolution
The formation, interpretation, and performance of this Agreement and any disputes arising out of it shall be governed by the substantive and procedural laws of Germany without regard to its rules on conflicts or choice of law and, to the extent applicable, the laws of Germany. The exclusive jurisdiction and venue for actions related to the subject matter hereof shall be the state and federal courts located in Berlin, Germany, and you hereby submit to the personal jurisdiction of such courts. You hereby waive any right to a jury trial in any proceeding arising out of or related to this Agreement. The United Nations Convention on Contracts for the International Sale of Goods does not apply to this Agreement.

## Changes and Amendments
We reserve the right to modify this Agreement or its policies relating to the Mobile Application or Services at any time, effective upon posting of an updated version of this Agreement. If we make material changes to this you will be promptly updated to provide you the opportunity to review the changes before they become effective.  Continued use of the Mobile Application after any such changes shall constitute your consent to such changes. If you object to any changes, you may uninstall the Application and request erasing your account.

## Acceptance of These Terms
You acknowledge that you have read this Agreement and agree to all its terms and conditions. By using HearMeNow you agree to be bound by this Agreement. If you do not agree to abide by the terms of this Agreement, you are not authorized to use or access HearMeNow.

## Termination
Both you and HearMeNow’s Developer may terminate this Agreement at any time with a notice to the other. On termination, you lose the right to access or use the Services. The following shall survive termination:

1. our rights to use and disclose your feedback;
2. sections “Limitation of Liability”, “Dispute Resolution”, “Refund Policy” and “Prohibited Uses” of this Agreement;
3. any amounts owed by either party prior to termination.

You can uninstall HearMeNow and send us an email to HearMeNowApp@gmail.com with the subject including words “Erase my account” to erase your account. You subscription to Premium Account for the next payment period can be cancelled via Google Play.

## Contacting Us
If you would like to contact us to understand more about this Agreement or wish to contact us concerning any matter relating to it, you may send an email to HearMeNowApp@gmail.com

This document was last updated on December 22, 2019
